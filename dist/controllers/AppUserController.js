"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../models/CommonModel/TableMaster");
const APIResponseModel_1 = require("../models/CommonModel/APIResponseModel");
const AppUserController = express_1.default();
AppUserController.post('/api/AppUser/SaveAppUserDetails', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("SaveAppUserDetails");
    // console.log("req.body"+req.body);
    try {
        const userModel = mongoose_1.default.model(TableMaster_1.TableMaster.AppUserTable);
        var requestObject = req.body;
        // console.log("req.body"+req.body);
        if (requestObject.Email != null && requestObject.Email != "") {
            // console.log(requestObject.Email);
            const appUser = yield userModel.getUserByEmailExist(requestObject.Email);
            //console.log(appUser);
            // console.log("appUser "+JSON.stringify(appUser)); 
            if (appUser.length > 0) {
                // console.log("appUser "+appUser.length); 
                return res.json({ status: false, Message: "User already exist" });
            }
            else {
                yield userModel.create(req.body);
                res.json({ status: true, Message: "User Created Successfully" });
            }
        }
        else {
            return res.json({ status: false, Message: "SOme Error Occured" });
        }
    }
    catch (e) {
        // console.log(e);
        return res.json({ status: false, Message: "Some Error Occured" });
    }
}));
AppUserController.post('/AppUser/LoginValidation', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("LoginValidation");
    const model = mongoose_1.default.model(TableMaster_1.TableMaster.AppUserTable);
    // console.log(req.body);
    const user = yield model.getUserByEmailAndPassword(req.body.userName, req.body.password);
    res.json(user);
}));
AppUserController.get('/api/AppUser/getAllUser', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // console.log("getAllProject enter "); 
        const model = mongoose_1.default.model(TableMaster_1.TableMaster.AppUserTable);
        const dbuser = yield model.getAllUser();
        let responseModel = new APIResponseModel_1.ResponseModel();
        // if(dbuser.length>0)
        // {
        responseModel.IsSuccess = true;
        responseModel.Message = "Fetch Successfully";
        responseModel.ResponseResult = dbuser;
        responseModel.AuthToken = "";
        // }
        console.log("dbProject " + JSON.stringify(responseModel));
        res.json(responseModel);
    }
    catch (e) {
        let responseModel;
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        responseModel.ResponseResult = [];
        responseModel.AuthToken = "";
        res.json(responseModel);
    }
}));
exports.default = AppUserController;
//# sourceMappingURL=AppUserController.js.map