"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../models/CommonModel/TableMaster");
const BookingContoller = express_1.default();
// list of all Project 
BookingContoller.post('/api/Booking/Create', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("Ctratebooking start");
    try {
        const project = mongoose_1.default.model(TableMaster_1.TableMaster.BookingTable);
        console.log("Ctratebooking " + JSON.stringify(req.body));
        var requestObject = req.body;
        //add validation
        yield project.create(req.body);
        console.log("Ctratebooking Booking Created Successfully");
        return res.json({ status: true, Message: "Booking Created Successfully" });
    }
    catch (e) {
        console.log("error " + e);
        return res.json({ status: false, Message: "Some Error Occured" });
    }
}));
BookingContoller.get('/api/Booking/GetBookingList', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("GetBookingList");
    const project = mongoose_1.default.model(TableMaster_1.TableMaster.BookingTable);
    const bookingList = yield project.getBookingList();
    res.json(bookingList);
}));
exports.default = BookingContoller;
//# sourceMappingURL=BookingController.js.map