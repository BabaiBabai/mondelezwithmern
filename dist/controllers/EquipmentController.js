"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../models/CommonModel/TableMaster");
const APIResponseModel_1 = require("../models/CommonModel/APIResponseModel");
const EquipmentController = express_1.default();
EquipmentController.post('/api/Equipment/CreateEquipment', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("CreateEquipment");
    let responseModel = new APIResponseModel_1.ResponseModel();
    try {
        const equipmentModel = mongoose_1.default.model(TableMaster_1.TableMaster.EquipmentTable);
        const model = mongoose_1.default.model(TableMaster_1.TableMaster.EquipmentTable);
        console.log(req.body);
        const equipment = yield model.GetEquipmentByEquipmentName(req.body.Equipment_Name);
        if (equipment != null && equipment.length > 0) {
            console.log('duplicate');
            responseModel.IsSuccess = false;
            responseModel.Message = "Equipment already exist";
        }
        else {
            yield equipmentModel.create(req.body);
            console.log('created');
            responseModel.IsSuccess = true;
            responseModel.Message = "created Successfully";
        }
        res.json(responseModel);
    }
    catch (e) {
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        res.json(responseModel);
    }
    //res.json({status: true});
}));
EquipmentController.post('/api/Equipment/GetEquipmentList', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("GetEquipmentList");
    const model = mongoose_1.default.model(TableMaster_1.TableMaster.EquipmentTable);
    const locations = yield model.GetEquipmentList();
    res.json(locations);
}));
EquipmentController.post('/api/Equipment/GetEquipmentByLocationId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("GetEquipmentByLocationId");
    const model = mongoose_1.default.model(TableMaster_1.TableMaster.EquipmentTable);
    // console.log(req.body);
    const equipments = yield model.GetEquipmentByLocationId(req.body.LocId);
    res.json(equipments);
}));
EquipmentController.post('/api/Equipment/GetEquipmentById', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("GetEquipmentById");
    const model = mongoose_1.default.model(TableMaster_1.TableMaster.EquipmentTable);
    // console.log(req.body);
    const equipments = yield model.GetEquipmentById(req.body.Id);
    console.log(equipments);
    res.json(equipments);
}));
exports.default = EquipmentController;
//# sourceMappingURL=EquipmentController.js.map