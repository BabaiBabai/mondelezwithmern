"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../models/CommonModel/TableMaster");
const APIResponseModel_1 = require("../models/CommonModel/APIResponseModel");
const LocationController = express_1.default();
LocationController.post('/api/Location/CreateLocation', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let responseModel = new APIResponseModel_1.ResponseModel();
    try {
        console.log("CreateLocation");
        console.log(req.body);
        const userModel = mongoose_1.default.model(TableMaster_1.TableMaster.LocationTable);
        const model = mongoose_1.default.model(TableMaster_1.TableMaster.LocationTable);
        const locationExist = yield model.GetLocationByLocationDesc(req.body.Desc);
        if (locationExist.length > 0) {
            responseModel.IsSuccess = false;
            responseModel.Message = "Location already exist";
        }
        else {
            yield userModel.create(req.body);
            responseModel.IsSuccess = true;
            responseModel.Message = "created Successfully";
        }
        res.json(responseModel);
        //res.json({status: true});
    }
    catch (e) {
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        res.json(responseModel);
    }
}));
LocationController.post('/api/Location/GetLocationList', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("GetLocationList");
    const model = mongoose_1.default.model(TableMaster_1.TableMaster.LocationTable);
    console.log(req.body);
    const locations = yield model.GetLocationList();
    res.json(locations);
}));
exports.default = LocationController;
//# sourceMappingURL=LocationController.js.map