"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../models/CommonModel/TableMaster");
const APIResponseModel_1 = require("../models/CommonModel/APIResponseModel");
const ProjectContoller = express_1.default();
// list of all Project
ProjectContoller.get('/api/project/getAllProject', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const project = mongoose_1.default.model(TableMaster_1.TableMaster.ProjectTable);
        const dbProject = yield project.GetProjectList();
        let responseModel = new APIResponseModel_1.ResponseModel();
        if (dbProject.length > 0) {
            responseModel.IsSuccess = true;
            responseModel.Message = "Fetch Successfully";
            responseModel.ResponseResult = dbProject;
            responseModel.AuthToken = "";
        }
        // console.log("dbProject "+JSON.stringify(responseModel));    
        res.json(responseModel);
    }
    catch (e) {
        let responseModel;
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        responseModel.ResponseResult = [];
        responseModel.AuthToken = "";
        res.json(responseModel);
    }
}));
ProjectContoller.post('/api/project/createProject', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("createProject start");   
    try {
        const project = mongoose_1.default.model(TableMaster_1.TableMaster.ProjectTable);
        // console.log("createProject "+JSON.stringify(req.body));    
        var requestObject = req.body;
        if (requestObject.ProjectNo != null && requestObject.ProjectNo != "") {
            const ProjectExist = yield project.GetProjectByProjectNo(requestObject.ProjectNo);
            //    console.log("ProjectExist "+ProjectExist);   
            //    console.log("ProjectExist "+JSON.stringify(ProjectExist));   
            if (ProjectExist.length > 0) {
                //    console.log("Project already exist");
                return res.json({ status: false, Message: "Project already exist" });
            }
            else {
                yield project.create(req.body);
                return res.json({ status: true, Message: "Project Created Successfully" });
            }
        }
        else {
            return res.json({ status: false, Message: "SOme Error Occured" });
        }
    }
    catch (e) {
        // console.log(e);
        return res.json({ status: false, Message: "Some Error Occured" });
    }
}));
ProjectContoller.post('/api/project/deleteProject', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("createProject start");   
    try {
        const project = mongoose_1.default.model(TableMaster_1.TableMaster.ProjectTable);
        console.log("createProject " + JSON.stringify(req.body));
        yield project.deleteOne(req.body);
        res.json({ status: true });
    }
    catch (e) {
        // console.log(e);
        res.json({ status: false });
    }
}));
exports.default = ProjectContoller;
//# sourceMappingURL=ProjectContoller.js.map