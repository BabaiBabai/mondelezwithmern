"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../models/CommonModel/TableMaster");
const APIResponseModel_1 = require("../models/CommonModel/APIResponseModel");
const TempBookingContoller = express_1.default();
TempBookingContoller.post('/api/TempBooking/Create', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let responseModel = new APIResponseModel_1.ResponseModel();
    console.log("CreatetempBooking start");
    try {
        const project = mongoose_1.default.model(TableMaster_1.TableMaster.TempBookingTable);
        console.log("CreatetempBooking " + JSON.stringify(req.body));
        var requestObject = req.body;
        console.log("req.body.StartTime " + req.body.StartTime);
        console.log("req.body.EndTime " + req.body.EndTime);
        //validation start
        if (req.body.StartTime > req.body.EndTime) {
            responseModel.IsSuccess = false;
            responseModel.Message = "end time should be gretter than start time";
            return res.json(responseModel);
        }
        if (Date.now() < req.body.StartTime) {
            responseModel.IsSuccess = false;
            responseModel.Message = "start time should be gretter than current time";
            return res.json(responseModel);
        }
        // const tempBookingList = mongoose.model<ItempBooking, ItempBookingModel>(TableMaster.TempBookingTable);
        // const list = await project.CheckTimeSlotIsAvailable(req.body.StartTime, req.body.EndTime, req.body.EquipmentId);
        // console.log("CheckTimeSlotIsAvailable length " + list.length);
        // if (list.length > 0) {
        //     responseModel.IsSuccess = false;
        //     responseModel.Message = "Slot is not  available";
        //     console.log("Slot available??" + responseModel.Message);
        // }
        // //Validation End
        // else {
        yield project.create(req.body);
        responseModel.IsSuccess = true;
        responseModel.Message = "Temp Booking Created Successfully";
        console.log(responseModel.Message);
        // }
        res.json(responseModel);
    }
    catch (e) {
        console.log(e);
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        console.log(responseModel.Message);
        res.json(responseModel);
    }
}));
TempBookingContoller.post('/api/TempBooking/DeletetempBooking', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let responseModel = new APIResponseModel_1.ResponseModel();
    console.log("DeletetempBooking start");
    try {
        const project = mongoose_1.default.model(TableMaster_1.TableMaster.TempBookingTable);
        console.log("DeletetempBooking " + JSON.stringify(req.body));
        yield project.deleteOne(req.body);
        responseModel.IsSuccess = true;
        responseModel.Message = "Temp Booking Created Successfully";
        res.json(responseModel);
    }
    catch (e) {
        console.log(e);
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        res.json(responseModel);
    }
}));
TempBookingContoller.get('/api/TempBooking/GetTempBookingList', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("GetEquipmentList");
    const model = mongoose_1.default.model(TableMaster_1.TableMaster.TempBookingTable);
    const locations = yield model.gettempBookingList();
    res.json(locations);
}));
exports.default = TempBookingContoller;
//# sourceMappingURL=TempBookingController.js.map