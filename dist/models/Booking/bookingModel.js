"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../CommonModel/TableMaster");
const BookingDetailsSchema = new mongoose_1.default.Schema({
    Equipment_id: { type: mongoose_1.default.Schema.Types.ObjectId, ref: TableMaster_1.TableMaster.EquipmentTable },
    BookingStartTime: mongoose_1.default.Schema.Types.Date,
    BookingEndTime: mongoose_1.default.Schema.Types.Date,
    Booking_Statusid: mongoose_1.default.Schema.Types.Number,
    Chkin_Status: mongoose_1.default.Schema.Types.Boolean,
    Chkin_Time: mongoose_1.default.Schema.Types.Date,
    Chkout_status: mongoose_1.default.Schema.Types.Boolean,
    Chkout_Time: mongoose_1.default.Schema.Types.Date
});
const BookingSchema = new mongoose_1.default.Schema({
    Booking_No: mongoose_1.default.Schema.Types.String,
    Project_Id: { type: mongoose_1.default.Schema.Types.ObjectId, ref: TableMaster_1.TableMaster.ProjectTable },
    User_Id: { type: mongoose_1.default.Schema.Types.ObjectId, ref: TableMaster_1.TableMaster.AppUserTable },
    IsActive: mongoose_1.default.Schema.Types.Date,
    Trial_Objective: mongoose_1.default.Schema.Types.String,
    BookingDetails: [BookingDetailsSchema]
});
BookingSchema.methods.toResponse = function () {
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
};
BookingSchema.statics.findBookingById = function (tempId) {
    const model = this;
    return model.find({ _id: tempId }).populate(TableMaster_1.TableMaster.BookingTable);
};
BookingSchema.statics.getBookingList = function () {
    const model = this;
    return model.find().populate(TableMaster_1.TableMaster.BookingTable);
};
exports.default = mongoose_1.default.model(TableMaster_1.TableMaster.BookingTable, BookingSchema);
//# sourceMappingURL=bookingModel.js.map