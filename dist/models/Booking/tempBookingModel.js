"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../CommonModel/TableMaster");
const tempBookingSchema = new mongoose_1.default.Schema({
    CreateTime: mongoose_1.default.Schema.Types.Date,
    UserId: { type: mongoose_1.default.Schema.Types.ObjectId, ref: TableMaster_1.TableMaster.AppUserTable },
    EquipmentId: { type: mongoose_1.default.Schema.Types.ObjectId, ref: TableMaster_1.TableMaster.EquipmentTable },
    StartTime: mongoose_1.default.Schema.Types.Date,
    EndTime: mongoose_1.default.Schema.Types.Date,
    RefNo: mongoose_1.default.Schema.Types.String
});
tempBookingSchema.methods.toResponse = function () {
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
};
tempBookingSchema.statics.findtempBookingById = function (tempId) {
    const model = this;
    return model.find({ _id: tempId }).populate(TableMaster_1.TableMaster.TempBookingTable);
};
tempBookingSchema.statics.findtempBookingByRefNo = function (refNo) {
    const model = this;
    return model.find({ RefNo: refNo }).populate(TableMaster_1.TableMaster.TempBookingTable);
};
tempBookingSchema.statics.gettempBookingList = function () {
    const model = this;
    return model.find().populate(TableMaster_1.TableMaster.TempBookingTable);
};
tempBookingSchema.statics.CheckTimeSlotIsAvailable = function (StartTime, EndTime, equpId) {
    const model = this;
    const tempModelList = this.gettempBookingList;
    return tempModelList.find(bookingItem => {
        ((StartTime <= bookingItem.EndTime
            &&
                EndTime >= StartTime) && bookingItem.EquipmentId === equpId);
    });
};
exports.default = mongoose_1.default.model(TableMaster_1.TableMaster.TempBookingTable, tempBookingSchema);
//# sourceMappingURL=tempBookingModel.js.map