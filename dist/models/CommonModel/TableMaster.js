"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableMaster = void 0;
exports.TableMaster = {
    LocationTable: "tbl_mst_location",
    EquipmentTable: "tbl_mst_equipments",
    AppUserTable: "tbl_app_users",
    ProjectTable: "tbl_projects",
    TempBookingTable: "tbl_equpment_booking_temp",
    BookingTable: "tbl_booking"
};
//# sourceMappingURL=TableMaster.js.map