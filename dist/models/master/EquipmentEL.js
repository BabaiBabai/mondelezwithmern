"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../CommonModel/TableMaster");
const EquipmentSchema = new mongoose_1.default.Schema({
    Equipment_Name: mongoose_1.default.Schema.Types.String,
    Location_Id: { type: mongoose_1.default.Schema.Types.ObjectId, ref: TableMaster_1.TableMaster.LocationTable },
    Min_Batch_Size: mongoose_1.default.Schema.Types.String,
    Max_Batch_Size: mongoose_1.default.Schema.Types.String,
    SetUp_Time: mongoose_1.default.Schema.Types.Decimal128,
    Process_Time: mongoose_1.default.Schema.Types.Decimal128,
    Cleaning_Time: mongoose_1.default.Schema.Types.Decimal128,
    Status_Id: mongoose_1.default.Schema.Types.Number,
    Category_Id: mongoose_1.default.Schema.Types.Number,
    IsDecommission: mongoose_1.default.Schema.Types.String,
    Comments: mongoose_1.default.Schema.Types.String,
    IsDelete: mongoose_1.default.Schema.Types.String,
});
EquipmentSchema.methods.toResponse = function () {
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
};
EquipmentSchema.statics.GetEquipmentById = function (id) {
    const model = this;
    return model.find({ _id: id }).populate(TableMaster_1.TableMaster.EquipmentTable);
};
EquipmentSchema.statics.GetEquipmentByLocationId = function (locationId) {
    return __awaiter(this, void 0, void 0, function* () {
        const model = this;
        return model.find({ Location_Id: locationId }).populate(TableMaster_1.TableMaster.EquipmentTable);
    });
};
EquipmentSchema.statics.GetEquipmentList = function () {
    return __awaiter(this, void 0, void 0, function* () {
        const model = this;
        const notes = yield model.find().populate(TableMaster_1.TableMaster.EquipmentTable);
        return notes;
    });
};
EquipmentSchema.statics.GetEquipmentByEquipmentName = function (equipmentName) {
    return __awaiter(this, void 0, void 0, function* () {
        const model = this;
        return model.find({ Equipment_Name: equipmentName }).populate(TableMaster_1.TableMaster.EquipmentTable);
    });
};
exports.default = mongoose_1.default.model(TableMaster_1.TableMaster.EquipmentTable, EquipmentSchema);
//# sourceMappingURL=EquipmentEL.js.map