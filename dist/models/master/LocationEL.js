"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../CommonModel/TableMaster");
const LocationSchema = new mongoose_1.default.Schema({
    Desc: mongoose_1.default.Schema.Types.String,
    IsDelete: mongoose_1.default.Schema.Types.Boolean
});
LocationSchema.methods.toResponse = function () {
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
};
LocationSchema.statics.getLocationById = function (id) {
    const model = this;
    return model.find({ _id: id }).populate(TableMaster_1.TableMaster.LocationTable);
};
LocationSchema.statics.GetLocationByLocationDesc = function (LocDesc) {
    const model = this;
    return model.find({ Desc: LocDesc }).populate(TableMaster_1.TableMaster.LocationTable);
};
LocationSchema.statics.findAllLocation = function () {
    return __awaiter(this, void 0, void 0, function* () {
        const model = this;
        const notes = yield model.find().populate(TableMaster_1.TableMaster.LocationTable);
        return notes;
    });
};
LocationSchema.statics.GetLocationList = function () {
    return __awaiter(this, void 0, void 0, function* () {
        const model = this;
        const notes = yield model.find().populate(TableMaster_1.TableMaster.LocationTable);
        return notes;
    });
};
exports.default = mongoose_1.default.model(TableMaster_1.TableMaster.LocationTable, LocationSchema);
//# sourceMappingURL=LocationEL.js.map