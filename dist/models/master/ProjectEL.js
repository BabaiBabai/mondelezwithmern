"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../CommonModel/TableMaster");
const ProjectSchema = new mongoose_1.default.Schema({
    ProjectNo: mongoose_1.default.Schema.Types.String,
    ProjectName: mongoose_1.default.Schema.Types.String,
    ProjectLead: mongoose_1.default.Schema.Types.String,
    IsDelete: mongoose_1.default.Schema.Types.String
});
ProjectSchema.methods.toResponse = function () {
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
};
ProjectSchema.statics.GetProjectById = function (id) {
    const model = this;
    return model.find({ _id: id }).populate(TableMaster_1.TableMaster.ProjectTable);
};
ProjectSchema.statics.GetProjectByProjectNo = function (projectNumber) {
    const model = this;
    return model.find({ ProjectNo: projectNumber }).populate(TableMaster_1.TableMaster.ProjectTable);
};
ProjectSchema.statics.GetProjectList = function () {
    return __awaiter(this, void 0, void 0, function* () {
        const model = this;
        const notes = yield model.find().populate(TableMaster_1.TableMaster.ProjectTable);
        return notes;
    });
};
exports.default = mongoose_1.default.model(TableMaster_1.TableMaster.ProjectTable, ProjectSchema);
//# sourceMappingURL=ProjectEL.js.map