"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TableMaster_1 = require("../CommonModel/TableMaster");
const userSchema = new mongoose_1.default.Schema({
    LocationId: mongoose_1.default.Schema.Types.Number,
    User_Password: mongoose_1.default.Schema.Types.String,
    uid: mongoose_1.default.Schema.Types.String,
    Name: mongoose_1.default.Schema.Types.String,
    Designation: mongoose_1.default.Schema.Types.String,
    Email: mongoose_1.default.Schema.Types.String,
    Profile_Image: mongoose_1.default.Schema.Types.String,
    Channel_Id: mongoose_1.default.Schema.Types.String,
    Nonce: mongoose_1.default.Schema.Types.String
});
userSchema.methods.toResponse = function () {
    const name = this.name;
    const data = this.toObject();
    data.id = data._id;
    data.name = name;
    delete data._id;
    delete data.__v;
    return data;
};
userSchema.statics.getUserByEmailAndPassword = function (Email, User_Password) {
    const model = this;
    return model.findOne({ Email, User_Password });
};
userSchema.statics.getUserByEmailExist = function (Email) {
    const model = this;
    return model.find({ Email }).populate(TableMaster_1.TableMaster.AppUserTable);
};
userSchema.statics.getAllUser = function () {
    const model = this;
    return model.find().populate(TableMaster_1.TableMaster.AppUserTable);
};
exports.default = mongoose_1.default.model(TableMaster_1.TableMaster.AppUserTable, userSchema);
//# sourceMappingURL=userModel.js.map