"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Util = void 0;
class Util {
    static cleanOutput(data) {
        if (Array.isArray(data)) {
            return data.map(d => this.cleanOutput(d));
        }
        else {
            data.id = data._id;
            delete data._id;
            delete data.__v;
            return data;
        }
    }
}
exports.Util = Util;
//# sourceMappingURL=util.js.map