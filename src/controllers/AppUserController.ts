import express from 'express';
import mongoose from 'mongoose';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';
import { Util } from '../utils/util';
import { IUser, IUserModel } from '../models/user/userModel';
import {TableMaster} from '../models/CommonModel/TableMaster';
import { ResponseModel } from '../models/CommonModel/APIResponseModel';

const AppUserController = express();

AppUserController.post('/api/AppUser/SaveAppUserDetails', async (req, res) => {
    // console.log("SaveAppUserDetails");
    // console.log("req.body"+req.body);
    try
    {
      const userModel = mongoose.model<IUser, IUserModel>(TableMaster.AppUserTable); 
      var requestObject= req.body;
      // console.log("req.body"+req.body);
      if(requestObject.Email!=null&& requestObject.Email!="")
      {  
        // console.log(requestObject.Email);
        const appUser=await userModel.getUserByEmailExist(requestObject.Email);
        //console.log(appUser);
        // console.log("appUser "+JSON.stringify(appUser)); 
        if(appUser.length>0)
        {
          // console.log("appUser "+appUser.length); 
          return res.json({status:false,Message:"User already exist"});
        }
        else
        {
          await userModel.create(req.body); 
          res.json({status: true,Message:"User Created Successfully"});
        }
      }
      else
      {
        return res.json({status:false,Message:"SOme Error Occured"});
      }
    }
    catch (e)
    {      
        // console.log(e);
        return  res.json({status:false,Message:"Some Error Occured"});
    } 
  });
  AppUserController.post('/AppUser/LoginValidation', async (req, res) => {
    console.log("LoginValidation");
    const model = mongoose.model<IUser, IUserModel>(TableMaster.AppUserTable);
    // console.log(req.body);
    const user = await model.getUserByEmailAndPassword(req.body.userName, req.body.password);
    res.json(user);
  });
  AppUserController.get('/api/AppUser/getAllUser', async (req, res) => {
  
    try
    {
    // console.log("getAllProject enter "); 
    const model=mongoose.model<IUser, IUserModel>(TableMaster.AppUserTable); 
    const dbuser= await model.getAllUser();
    let responseModel:ResponseModel=new ResponseModel();
    // if(dbuser.length>0)
    // {
        responseModel.IsSuccess=true;
        responseModel.Message="Fetch Successfully";
        responseModel.ResponseResult=dbuser;
        responseModel.AuthToken="";       
    // }
    console.log("dbProject "+JSON.stringify(responseModel));    
    res.json(responseModel);
    }
    catch (e)
    {      
        let responseModel:any;
        responseModel.IsSuccess=false;
        responseModel.Message="Error Occured";
        responseModel.ResponseResult=[];
        responseModel.AuthToken="";
        res.json(responseModel);
    }
});
export default AppUserController;