import express from 'express';
import mongoose from 'mongoose';
import { TableMaster } from '../models/CommonModel/TableMaster';
import { IBookingModel, IBooking } from '../models/Booking/bookingModel';
import { ResponseModel } from '../models/CommonModel/APIResponseModel'
import { stringify } from 'querystring';

const BookingContoller = express();

// list of all Project 

BookingContoller.post('/api/Booking/Create', async (req, res) => {

    console.log("Ctratebooking start");
    try {
        const project = mongoose.model<IBooking, IBookingModel>(TableMaster.BookingTable);
        console.log("Ctratebooking " + JSON.stringify(req.body));
        var requestObject = req.body;
        //add validation

        await project.create(req.body);
        
        console.log("Ctratebooking Booking Created Successfully" );
        return res.json({ status: true, Message: "Booking Created Successfully" });
    }
    catch (e) {
        console.log("error "+e);

        return res.json({ status: false, Message: "Some Error Occured" });
    }
});
BookingContoller.get('/api/Booking/GetBookingList', async (req, res) => {
    console.log("GetBookingList");
    const project = mongoose.model<IBooking, IBookingModel>(TableMaster.BookingTable);
    const bookingList = await project.getBookingList();
    res.json(bookingList);
  });

export default BookingContoller;