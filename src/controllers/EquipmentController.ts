 
import express from 'express';
import mongoose from 'mongoose';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';
import { Util } from '../utils/util';
import { IEquipment, IEquipmentModel } from '../models/master/EquipmentEL';
import {TableMaster} from '../models/CommonModel/TableMaster'
import {ResponseModel} from '../models/CommonModel/APIResponseModel';

const EquipmentController = express();

EquipmentController.post('/api/Equipment/CreateEquipment', async (req, res) => {
    console.log("CreateEquipment");
    let responseModel:ResponseModel=new ResponseModel(); 
    try{
    const equipmentModel = mongoose.model(TableMaster.EquipmentTable);  
    const model = mongoose.model<IEquipment, IEquipmentModel>(TableMaster.EquipmentTable);
    console.log(req.body);
    const equipment = await model.GetEquipmentByEquipmentName(req.body.Equipment_Name);
  
    if(equipment!=null && equipment.length>0){
      console.log('duplicate');
      responseModel.IsSuccess=false;
      responseModel.Message="Equipment already exist"; 
    }
    else{
      await equipmentModel.create(req.body); 
      console.log('created')
        responseModel.IsSuccess=true;
        responseModel.Message="created Successfully"; 
    }  
    res.json(responseModel);
  }
  catch(e){
    responseModel.IsSuccess=false;
    responseModel.Message="Error Occured"; 
    res.json(responseModel);
  }
    //res.json({status: true});
  });

  EquipmentController.post('/api/Equipment/GetEquipmentList', async (req, res) => {
    // console.log("GetEquipmentList");
    const model = mongoose.model<IEquipment, IEquipmentModel>(TableMaster.EquipmentTable); 
    const locations = await model.GetEquipmentList();
    res.json(locations);
  });
  EquipmentController.post('/api/Equipment/GetEquipmentByLocationId', async (req, res) => {
    // console.log("GetEquipmentByLocationId");
    const model = mongoose.model<IEquipment, IEquipmentModel>(TableMaster.EquipmentTable);
    // console.log(req.body);
    const equipments = await model.GetEquipmentByLocationId(req.body.LocId);
    res.json(equipments);
  });
  EquipmentController.post('/api/Equipment/GetEquipmentById', async (req, res) => {
    // console.log("GetEquipmentById");
    const model = mongoose.model<IEquipment, IEquipmentModel>(TableMaster.EquipmentTable);
    // console.log(req.body);
    const equipments = await model.GetEquipmentById(req.body.Id);
    console.log(equipments);
    res.json(equipments);
  });

  export default EquipmentController;