 
import express from 'express';
import mongoose from 'mongoose';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';
import { Util } from '../utils/util';
import { ILocation, ILocationModel } from '../models/master/LocationEL';
import {TableMaster} from '../models/CommonModel/TableMaster';
import {ResponseModel} from '../models/CommonModel/APIResponseModel';
 

const LocationController = express();

LocationController.post('/api/Location/CreateLocation', async (req, res) => {
  let responseModel:ResponseModel=new ResponseModel(); 
 try{
    console.log("CreateLocation");
    console.log(req.body)
    const userModel = mongoose.model(TableMaster.LocationTable); 
    const model = mongoose.model<ILocation, ILocationModel>(TableMaster.LocationTable);
    const locationExist=  await model.GetLocationByLocationDesc(req.body.Desc);
    if(locationExist.length>0){
      responseModel.IsSuccess=false;
      responseModel.Message="Location already exist"; 
    }
    else{
        await userModel.create(req.body); 
        responseModel.IsSuccess=true;
        responseModel.Message="created Successfully"; 
    }  
    res.json(responseModel);
    //res.json({status: true});
 }
 catch (e)
    {    
        responseModel.IsSuccess=false;
        responseModel.Message="Error Occured"; 
        res.json(responseModel);
    }
  });
  LocationController.post('/api/Location/GetLocationList', async (req, res) => {
    console.log("GetLocationList");
    const model = mongoose.model<ILocation, ILocationModel>(TableMaster.LocationTable);
    console.log(req.body);
    const locations = await model.GetLocationList();
    res.json(locations);
  });
   
  export default LocationController;