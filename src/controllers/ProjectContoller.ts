import express from 'express';
import mongoose from 'mongoose'; 
import { TableMaster } from '../models/CommonModel/TableMaster';
import { IProjectModel, IProject } from '../models/master/ProjectEL';
 import {ResponseModel} from '../models/CommonModel/APIResponseModel'
import { stringify } from 'querystring';

const ProjectContoller=express();
  
// list of all Project
ProjectContoller.get('/api/project/getAllProject', async (req, res) => {
  
    try
    {
    
    const project=mongoose.model<IProject, IProjectModel>(TableMaster.ProjectTable); 
    const dbProject= await project.GetProjectList();
    let responseModel:ResponseModel=new ResponseModel();
    if(dbProject.length>0)
    {
        responseModel.IsSuccess=true;
        responseModel.Message="Fetch Successfully";
        responseModel.ResponseResult=dbProject;
        responseModel.AuthToken="";       
    }
    // console.log("dbProject "+JSON.stringify(responseModel));    
    res.json(responseModel);
    }
    catch (e)
    {      
        let responseModel:any;
        responseModel.IsSuccess=false;
        responseModel.Message="Error Occured";
        responseModel.ResponseResult=[];
        responseModel.AuthToken="";
        res.json(responseModel);
    }
});


ProjectContoller.post('/api/project/createProject',async (req,res)=>{
    
    // console.log("createProject start");   
    try
    {
        const project=mongoose.model<IProject, IProjectModel>(TableMaster.ProjectTable); 
        // console.log("createProject "+JSON.stringify(req.body));    
        var requestObject= req.body;
        if(requestObject.ProjectNo!=null&& requestObject.ProjectNo!="")
        {
           const ProjectExist=  await project.GetProjectByProjectNo(requestObject.ProjectNo);
        //    console.log("ProjectExist "+ProjectExist);   
        //    console.log("ProjectExist "+JSON.stringify(ProjectExist));   
           if(ProjectExist.length>0)
           {
            //    console.log("Project already exist");
               return res.json({status:false,Message:"Project already exist"});
           }
           else
           {
            await project.create(req.body);
            return res.json({status:true,Message:"Project Created Successfully"});
           }
          
        }
        else
        {
            return res.json({status:false,Message:"SOme Error Occured"});
        }      
    }
    catch (e)
    {      
        // console.log(e);
        return  res.json({status:false,Message:"Some Error Occured"});
    }   
});
 
ProjectContoller.post('/api/project/deleteProject',async (req,res)=>{
    
    // console.log("createProject start");   
    try
    {
        const project=mongoose.model(TableMaster.ProjectTable); 
        console.log("createProject "+JSON.stringify(req.body));      
        await project.deleteOne(req.body);
        res.json({status:true});
    }
    catch (e)
    {      
        // console.log(e);
        res.json({status:false});
    }   
});
export default ProjectContoller;