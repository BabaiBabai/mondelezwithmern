import express from 'express';
import mongoose from 'mongoose';
import { TableMaster } from '../models/CommonModel/TableMaster';
import { ItempBookingModel, ItempBooking } from '../models/Booking/tempBookingModel';
import { ResponseModel } from '../models/CommonModel/APIResponseModel'
import { stringify } from 'querystring';

const TempBookingContoller = express();



TempBookingContoller.post('/api/TempBooking/Create', async (req, res) => {
    let responseModel: ResponseModel = new ResponseModel();
    console.log("CreatetempBooking start");
    try {
        const project = mongoose.model<ItempBooking, ItempBookingModel>(TableMaster.TempBookingTable);
        console.log("CreatetempBooking " + JSON.stringify(req.body));
        var requestObject = req.body;
        console.log("req.body.StartTime "+ req.body.StartTime);
        console.log("req.body.EndTime "+ req.body.EndTime);
        //validation start
        if (req.body.StartTime > req.body.EndTime) {
            responseModel.IsSuccess = false;
            responseModel.Message = "end time should be gretter than start time";
            return res.json(responseModel);
        }
        if (Date.now() < req.body.StartTime) {
            responseModel.IsSuccess = false;
            responseModel.Message = "start time should be gretter than current time";
            return res.json(responseModel);
        }

        // const tempBookingList = mongoose.model<ItempBooking, ItempBookingModel>(TableMaster.TempBookingTable);
        // const list = await project.CheckTimeSlotIsAvailable(req.body.StartTime, req.body.EndTime, req.body.EquipmentId);
        // console.log("CheckTimeSlotIsAvailable length " + list.length);
        // if (list.length > 0) {
        //     responseModel.IsSuccess = false;
        //     responseModel.Message = "Slot is not  available";
        //     console.log("Slot available??" + responseModel.Message);

        // }
        // //Validation End
        // else {
            await project.create(req.body);
            responseModel.IsSuccess = true;
            responseModel.Message = "Temp Booking Created Successfully";
            console.log(responseModel.Message);
        // }
        res.json(responseModel);
    }
    catch (e) {
        console.log(e);
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        console.log(responseModel.Message);
        res.json(responseModel);
    }
});

TempBookingContoller.post('/api/TempBooking/DeletetempBooking', async (req, res) => {
    let responseModel: ResponseModel = new ResponseModel();
    console.log("DeletetempBooking start");
    try {
        const project = mongoose.model(TableMaster.TempBookingTable);
        console.log("DeletetempBooking " + JSON.stringify(req.body));
        await project.deleteOne(req.body);
        responseModel.IsSuccess = true;
        responseModel.Message = "Temp Booking Created Successfully";
        res.json(responseModel);
    }
    catch (e) {
        console.log(e);
        responseModel.IsSuccess = false;
        responseModel.Message = "Error Occured";
        res.json(responseModel);
    }
});
TempBookingContoller.get('/api/TempBooking/GetTempBookingList', async (req, res) => {
    // console.log("GetEquipmentList");
    const model = mongoose.model<ItempBooking, ItempBookingModel>(TableMaster.TempBookingTable);
    const locations = await model.gettempBookingList();
    res.json(locations);
  });

export default TempBookingContoller;