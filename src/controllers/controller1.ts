import express from 'express';

const controller1 = express();

controller1.get('/controller1/first', (req, res) => {
  const list = [
    'One',
    'Two',
    'Three',
    'Four',
    'Five'
  ];
  res.render('index', {
    numbers: list,
    pageName: 'Numbers'
  });
});

controller1.get('/controller1/second', (req, res) => {
  res.render('sub-views/file');
});

controller1.post('/controller1/login', (req, res) => {

  const reqBody = req.body;
  const userName = 'user@mail.com';
  const password = 'password@123';

  if (userName === reqBody.email && password === reqBody.password) {
    res.send('Welcome');
  } else {
    res.send('Login Failed');
  }
});

export default controller1;