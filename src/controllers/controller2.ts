import express from 'express';

const controller2 = express();

controller2.get('/controller2/first', (req, res) => {
  console.log(req.query);
  res.send('Response from first route in controller 2');
});

controller2.get('/controller2/second', (req, res) => {
  res.send('Response from 2nd route in controller 2');
});

export default controller2;