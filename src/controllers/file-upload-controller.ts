import express from 'express';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';

const fileUploadController = express();
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (file.mimetype === 'image/png') {
      cb(null, './uploads');
    } else {
      cb(Error('File type not allowed'), '');
    }
  },
  // file.1.png -> [file - 0, 1 - 1, png - 2] -> 3 -> 3-1 = 2
  filename: (req, file, cb) => {
    const fileName = file.originalname;
    const fileParts = fileName.split(/\./g);
    const ext = fileParts[fileParts.length - 1];
    cb(null, uuidv4() + '.' + ext);
  }
})
const uploader = multer({storage});


fileUploadController.post('/image-upload', uploader.single('image'), async (req, res) => {
  console.log(req.file);
  console.log('File uploaded....');
  const resizedFile = sharp(path.join(req.file.destination, req.file.filename));
  await resizedFile.resize(200).toFile(path.join(req.file.destination, uuidv4() + '.jpg'));
  fs.unlinkSync(path.join(req.file.destination, req.file.filename));
  res.send('Done');
});

export default fileUploadController;