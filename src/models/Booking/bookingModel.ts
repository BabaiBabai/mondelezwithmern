import mongoose from 'mongoose';
import {TableMaster} from '../CommonModel/TableMaster';
const BookingDetailsSchema = new mongoose.Schema({
    
    Equipment_id: {type: mongoose.Schema.Types.ObjectId, ref: TableMaster.EquipmentTable},
    BookingStartTime:mongoose.Schema.Types.Date,
    BookingEndTime:mongoose.Schema.Types.Date,
    Booking_Statusid:mongoose.Schema.Types.Number,
    Chkin_Status: mongoose.Schema.Types.Boolean ,
    Chkin_Time: mongoose.Schema.Types.Date,
    Chkout_status:mongoose.Schema.Types.Boolean,
    Chkout_Time:mongoose.Schema.Types.Date
     
  });  

  export interface IBookingDetails extends mongoose.Document { 
    Equipment_id: any;
    Project_Id: any;
    User_Id: any;
    BookingStartTime:Date;
    BookingEndTime:Date;
    Booking_Statusid: number; 
    Chkin_Status:false,
    Chkin_Time:Date;
    Chkout_status:false;
    Chkout_Time: Date;      
    toResponse(): IBookingDetails;
  }
const BookingSchema = new mongoose.Schema({
    
    Booking_No: mongoose.Schema.Types.String,
    Project_Id:{type: mongoose.Schema.Types.ObjectId, ref: TableMaster.ProjectTable},
    User_Id:{type: mongoose.Schema.Types.ObjectId, ref: TableMaster.AppUserTable},
    IsActive:mongoose.Schema.Types.Date,
    Trial_Objective: mongoose.Schema.Types.String ,
    BookingDetails:[BookingDetailsSchema]
  });
  export interface IBooking extends mongoose.Document { 
    id?: any;
    Project_Id: any;
    User_Id: any;
    IsActive:Date;
    Trial_Objective:Date;
    Booking_No: string; 
    BookingDetails:IBooking,
    toResponse(): IBooking;
  }
  BookingSchema.methods.toResponse = function() { 
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
  }
   
   
  BookingSchema.statics.findBookingById = function(tempId: any) {
    const model: IBookingModel = this; 
    return model.find({_id: tempId}).populate(TableMaster.BookingTable);
  }
   
  BookingSchema.statics.getBookingList = function() {
    const model: IBookingModel = this; 
    return model.find().populate(TableMaster.BookingTable);
  }
  export interface IBookingModel extends mongoose.Model<IBooking> {
    // methods
    findBookingById(id:any): Promise<IBooking[]>; 
    getBookingList(): Promise<IBooking[]>; 
  }
  
  export default mongoose.model<IBooking, IBookingModel>(TableMaster.BookingTable, BookingSchema);