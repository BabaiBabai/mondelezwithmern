import mongoose from 'mongoose';
import {TableMaster} from '../CommonModel/TableMaster';

const tempBookingSchema = new mongoose.Schema({
    
    CreateTime: mongoose.Schema.Types.Date,
    UserId:{type: mongoose.Schema.Types.ObjectId, ref: TableMaster.AppUserTable},
    EquipmentId:{type: mongoose.Schema.Types.ObjectId, ref: TableMaster.EquipmentTable},
    StartTime:mongoose.Schema.Types.Date,
    EndTime: mongoose.Schema.Types.Date,
    RefNo: mongoose.Schema.Types.String 
  });
  export interface ItempBooking extends mongoose.Document { 
    id?: any;
    UserId: any;
    EquipmentId: any;
    StartTime:Date;
    EndTime:Date;
    CreateTime:Date;
    RefNo: string; 
    toResponse(): ItempBooking;
  }
  
  tempBookingSchema.methods.toResponse = function() { 
    const data = this.toObject();
    data.id = data._id; 
    delete data._id;
    delete data.__v;
    return data;
  }
   
  tempBookingSchema.statics.findtempBookingById = function(tempId: any) {
    const model: ItempBookingModel = this; 
    return model.find({_id: tempId}).populate(TableMaster.TempBookingTable);
  }
  tempBookingSchema.statics.findtempBookingByRefNo = function(refNo: string) {
    const model: ItempBookingModel = this; 
    return model.find({RefNo: refNo}).populate(TableMaster.TempBookingTable);
  }
  tempBookingSchema.statics.gettempBookingList = function() {
    const model: ItempBookingModel = this; 
    return model.find().populate(TableMaster.TempBookingTable);
  }
  tempBookingSchema.statics.CheckTimeSlotIsAvailable = function(StartTime: Date,EndTime:Date,equpId:any) {
    const model: ItempBookingModel = this; 
    const tempModelList: ItempBooking[] = this.gettempBookingList; 
    return tempModelList.find(bookingItem => {
      ( (
            StartTime <= bookingItem.EndTime
            &&
            EndTime >= StartTime
        ) && bookingItem.EquipmentId===equpId
        )
    })
  }
 
  //bookingItem.EquipmentId===equpId && bookingItem.CreateTime.add(10, 'minutes')< Date.now()
  export interface ItempBookingModel extends mongoose.Model<ItempBooking> {
    // methods 
    findtempBookingById(id:any): Promise<ItempBooking[]>;
    findtempBookingByRefNo(refno:string): Promise<ItempBooking[]>;
    CheckTimeSlotIsAvailable(StartTime: Date,EndTime:Date,equpId:any): Promise<ItempBooking[]>;
    gettempBookingList(): Promise<ItempBooking[]>;
    
  }
  
  export default mongoose.model<ItempBooking, ItempBookingModel>(TableMaster.TempBookingTable, tempBookingSchema);