import mongoose from 'mongoose';
import {TableMaster} from '../CommonModel/TableMaster'
const EquipmentSchema = new mongoose.Schema({
    Equipment_Name: mongoose.Schema.Types.String,
    Location_Id: {type: mongoose.Schema.Types.ObjectId, ref: TableMaster.LocationTable}, 
    Min_Batch_Size: mongoose.Schema.Types.String,
    Max_Batch_Size: mongoose.Schema.Types.String ,
    SetUp_Time: mongoose.Schema.Types.Decimal128,
    Process_Time: mongoose.Schema.Types.Decimal128 ,
    Cleaning_Time: mongoose.Schema.Types.Decimal128,
    Status_Id: mongoose.Schema.Types.Number ,
    Category_Id: mongoose.Schema.Types.Number,
    IsDecommission: mongoose.Schema.Types.String ,
    Comments: mongoose.Schema.Types.String ,
    IsDelete: mongoose.Schema.Types.String ,
  });
  export interface IEquipment extends mongoose.Document {
    id?: string;
    Equipment_Name: string;
    Location_Id: any;
    Min_Batch_Size: any;
    Max_Batch_Size: any;
    SetUp_Time: any;
    Process_Time: any;
    Cleaning_Time: any;
    Status_Id: any;
    Category_Id: any;
    IsDecommission: boolean;
    Comments: any;
    IsDelete: boolean;

    // methods
    //eligibe(): boolean;
    toResponse(): IEquipment;
  }
  
  EquipmentSchema.methods.toResponse = function() { 
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
  }
  EquipmentSchema.statics.GetEquipmentById = function(id: any) {
    const model: IEquipmentModel = this; 
    return model.find({_id: id}).populate(TableMaster.EquipmentTable);
  }
  EquipmentSchema.statics.GetEquipmentByLocationId = async function(locationId:any) {
    const model: IEquipmentModel = this; 
    return model.find({Location_Id: locationId}).populate(TableMaster.EquipmentTable);
  }
  EquipmentSchema.statics.GetEquipmentList = async function() {
    const model: IEquipmentModel = this;
    const notes = await model.find().populate(TableMaster.EquipmentTable); 
    return notes;
  }
  EquipmentSchema.statics.GetEquipmentByEquipmentName = async function(equipmentName:any) {
    const model: IEquipmentModel = this; 
    return model.find({Equipment_Name: equipmentName}).populate(TableMaster.EquipmentTable);
  }
  export interface IEquipmentModel extends mongoose.Model<IEquipment> {
    // methods
    GetEquipmentById(id: any): Promise<IEquipment>;
    GetEquipmentList(): Promise<IEquipment[]>;
    GetEquipmentByLocationId(locationId:any): Promise<IEquipment[]>;
    GetEquipmentByEquipmentName(equipmentName:any):IEquipment[]; 
  }
  
  export default mongoose.model<IEquipment, IEquipmentModel>(TableMaster.EquipmentTable, EquipmentSchema);