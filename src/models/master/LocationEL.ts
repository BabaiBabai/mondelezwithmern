import mongoose from 'mongoose';
import {TableMaster} from '../CommonModel/TableMaster'
const LocationSchema = new mongoose.Schema({
    Desc: mongoose.Schema.Types.String,
    IsDelete: mongoose.Schema.Types.Boolean 
  });
  export interface ILocation extends mongoose.Document {
    id?: string;
    Desc: string;
    IsDelete: boolean  
    // methods
    //eligibe(): boolean;
    toResponse(): ILocation;
  }
  
  LocationSchema.methods.toResponse = function() { 
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
  }
  LocationSchema.statics.getLocationById = function(id: any) {
    const model: ILocationModel = this;
    
  return model.find({_id: id}).populate(TableMaster.LocationTable);
  }
  LocationSchema.statics.GetLocationByLocationDesc = function(LocDesc: any) {
    const model: ILocationModel = this; 
    return model.find({Desc: LocDesc}).populate(TableMaster.LocationTable);
  }
  LocationSchema.statics.findAllLocation = async function() {
    const model: ILocationModel = this;
    const notes = await model.find().populate(TableMaster.LocationTable); 
    return notes;
  }
  LocationSchema.statics.GetLocationList = async function() {
    const model: ILocationModel = this;
    const notes = await model.find().populate(TableMaster.LocationTable); 
    return notes;
  }
  
  export interface ILocationModel extends mongoose.Model<ILocation> {
    // methods
    getLocationById(id: any): Promise<ILocation>;
    GetLocationByLocationDesc(LocDesc:any): ILocation[]; 
    GetLocationList():Promise<ILocation[]>;

  }
  
  export default mongoose.model<ILocation, ILocationModel>(TableMaster.LocationTable, LocationSchema);