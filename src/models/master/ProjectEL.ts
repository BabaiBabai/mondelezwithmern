import mongoose from 'mongoose';
import {TableMaster} from '../CommonModel/TableMaster'
const ProjectSchema = new mongoose.Schema({
     ProjectNo:mongoose.Schema.Types.String,
    ProjectName:mongoose.Schema.Types.String,
    ProjectLead:mongoose.Schema.Types.String,
    IsDelete:mongoose.Schema.Types.String 
  });
  export interface IProject extends mongoose.Document {
    id?: string;
    ProjectNo: string;
    ProjectName: any;
    ProjectLead: any;
    IsDelete: boolean;  
    toResponse(): IProject;
  }
  
  ProjectSchema.methods.toResponse = function() { 
    const data = this.toObject();
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
  }
  ProjectSchema.statics.GetProjectById = function(id: any) {
    const model: IProjectModel = this; 
    return model.find({_id: id}).populate(TableMaster.ProjectTable);
  }
  ProjectSchema.statics.GetProjectByProjectNo = function(projectNumber: any) {
    const model: IProjectModel = this; 
    return model.find({ProjectNo: projectNumber}).populate(TableMaster.ProjectTable);
  }
  ProjectSchema.statics.GetProjectList = async function() {
    const model: IProjectModel = this;
    const notes = await model.find().populate(TableMaster.ProjectTable); 
    return notes;
  }
  
  export interface IProjectModel extends mongoose.Model<IProject> {
    // methods
    GetProjectById(id: any): Promise<IProject>;
    GetProjectList(): Promise<IProject[]>;
    GetProjectByProjectNo(projectNumber:any):IProject[];
  }
  
  export default mongoose.model<IProject, IProjectModel>(TableMaster.ProjectTable, ProjectSchema);