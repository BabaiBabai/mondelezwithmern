import mongoose from 'mongoose';
import { IUser } from './user-model';

const noteSchema = new mongoose.Schema({
  title: mongoose.Schema.Types.String,
  body: mongoose.Schema.Types.String,
  author: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  noteColor:mongoose.Schema.Types.String
});

export interface INote extends mongoose.Document {
  title: string;
  body: string;
  author: any | IUser;
  noteColor:string
}

noteSchema.statics.findAllNotes = async function() {
  const model: INoteModel = this;
  const notes = await model.find().populate('author');
  const notesObj = notes.map(n => {
    let name = null;
    if (n.author) {
      name = n.author.name;
    }
    const nn = n.toObject();
    if (nn.author) {
      nn.author = name;
    }
    return nn;
  });
  return notesObj;
}

noteSchema.statics.findNotesByAuther = function(autherId: string) {
  const model: INoteModel = this;
  return model.find({author: autherId}).populate('author');
}
noteSchema.statics.findNotesByNoteId = function(noteid: string) {
  const model: INoteModel = this;
  return model.find({_id: noteid}).populate('author');
}

export interface INoteModel extends mongoose.Model<INote> {
  findAllNotes(): Promise<INote[]>;
  findNotesByAuther(autherId: string): Promise<INote[]>;
  findNotesByNoteId(noteid:string): Promise<INote[]>;
}

export default mongoose.model<INote, INoteModel>('Note', noteSchema);