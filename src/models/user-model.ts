import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  firstName: mongoose.Schema.Types.String,
  lastName: mongoose.Schema.Types.String,
  email:mongoose.Schema.Types.String,
  mobile:mongoose.Schema.Types.Number,
  password: mongoose.Schema.Types.String,
  profileImage: mongoose.Schema.Types.String

});

userSchema.virtual('name').get(function() {
  // @ts-ignore
  return this.firstName + ' ' + this.lastName;
});

userSchema.methods.eligibe = function() {
  return this.age >= 20;
}

userSchema.methods.toResponse = function() {
  const name = this.name;
  const data = this.toObject();
  data.id = data._id;
  data.name = name;
  delete data._id;
  delete data.__v;
  return data;
}

// userSchema.statics.getUserByUserNameAndPassword = function(userName: string, password: string) {
//   const model: IUserModel = this;
//   return model.findOne({userName, password});
// }
userSchema.statics.getUserByEmailAndPassword = function(email: string, password: string) {
  const model: IUserModel = this;
  return model.findOne({email, password});
}

export interface IUser extends mongoose.Document {
  id?: string;
  firstName: string;
  lastName: string;
  email:string;
  mobile:string;
  password: string;
  profileImage?: string;
  // virtuals
  name?: string;

  // methods
  eligibe(): boolean;
  toResponse(): IUser;
}

export interface IUserModel extends mongoose.Model<IUser> {
  // methods
  getUserByEmailAndPassword(email: string, password: string): Promise<IUser>;
}

export default mongoose.model<IUser, IUserModel>('User', userSchema);