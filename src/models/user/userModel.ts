import mongoose from 'mongoose';
import {TableMaster} from '../CommonModel/TableMaster';

const userSchema = new mongoose.Schema({
    
    LocationId: mongoose.Schema.Types.Number,
    User_Password:mongoose.Schema.Types.String,
    uid:mongoose.Schema.Types.String,
    Name:mongoose.Schema.Types.String,
    Designation: mongoose.Schema.Types.String,
    Email: mongoose.Schema.Types.String,
    Profile_Image: mongoose.Schema.Types.String,
    Channel_Id: mongoose.Schema.Types.String,
    Nonce: mongoose.Schema.Types.String 
  });
  export interface IUser extends mongoose.Document {
    id?: string;
    User_Name: string;
    LocationId?: number;
    uid:string;
    Name:string;
    Designation: string;
    Email: string;
    Profile_Image: string;
    Channel_Id: string;
    Nonce: string;
    
    // methods
    //eligibe(): boolean;
    toResponse(): IUser;
  }
  
  userSchema.methods.toResponse = function() {
    const name = this.name;
    const data = this.toObject();
    data.id = data._id;
    data.name = name;
    delete data._id;
    delete data.__v;
    return data;
  }
  userSchema.statics.getUserByEmailAndPassword = function(Email: string, User_Password: string) {
    const model: IUserModel = this;
    return model.findOne({Email, User_Password});
  }
  
  userSchema.statics.getUserByEmailExist = function(Email: string) {
    const model: IUserModel = this;
    return model.find({Email}).populate(TableMaster.AppUserTable);
  }
  userSchema.statics.getAllUser = function() {
    const model: IUserModel = this;
    return model.find().populate(TableMaster.AppUserTable); 
  }
  export interface IUserModel extends mongoose.Model<IUser> {
    // methods
    getUserByEmailAndPassword(Email: string, User_Password: string): Promise<IUser>;
    getUserByEmailExist(Email: string): IUser[];
    getAllUser(): Promise<IUser>;
  }
  
  export default mongoose.model<IUser, IUserModel>(TableMaster.AppUserTable, userSchema);