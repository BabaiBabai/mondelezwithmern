export class Util {
  static cleanOutput(data: any): any {
    if (Array.isArray(data)) {
      return data.map(d => this.cleanOutput(d));
    } else {
      data.id = data._id;
      delete data._id;
      delete data.__v;
      return data;
    }
  }
}